﻿using System.Web.Http;
using Swashbuckle.Application;

namespace WebPreviewAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.Routes.MapHttpRoute(
                "swagger_root",
                "",
                null,
                null,
                new RedirectHandler(message => message.RequestUri.ToString(), "swagger"));
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new { id = RouteParameter.Optional }
            );
        }
    }
}