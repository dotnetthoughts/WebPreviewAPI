﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using NReadability;
using Swashbuckle.Swagger.Annotations;
using WebPreviewAPI.Models;

namespace WebPreviewAPI.Controllers
{
    /// <summary>
    /// Helps to create extract clean contents from a web page.
    /// </summary>
    /// <remarks>
    /// Helps to create extract clean contents from a web page.
    /// </remarks>
    [Route("api/preview")]
    public class PreviewController : ApiController
    {
        /// <summary>
        /// Returns preview json.
        /// </summary>
        /// <param name="url">Web page URL to be fetched.</param>
        /// <returns>Preview JSON object</returns>
        /// <remarks>Return web page title and content as JSON</remarks>
        /// <exception cref="HttpStatusCode.NotFound">Thrown when URL is missing</exception>
        /// <exception cref="HttpStatusCode.BadRequest">Thrown when URL format is not valid.</exception>
        /// <exception cref="HttpStatusCode.NoContent">Thrown when unable to fetch contents from the URL.</exception>
        /// <exception cref="HttpStatusCode.InternalServerError">Thrown when an exception occured.</exception>
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(Preview))]
        public HttpResponseMessage Get([FromUri] string url)
        {
            if (string.IsNullOrWhiteSpace(url))
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "URL missing.");

            if (!IsValidURL(url))
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "URL format is not valid.");

            try
            {
                var transcoder = new NReadabilityWebTranscoder();
                var transcodeResult = transcoder.Transcode(new WebTranscodingInput(url));

                if (!transcodeResult.ContentExtracted)
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, $"Unable to fetch content from {url}");

                var previewResult = new Preview
                {
                    Content = transcodeResult.ExtractedContent,
                    Title = transcodeResult.TitleExtracted ? transcodeResult.ExtractedTitle : "No Title.",
                };

                var response = Request.CreateResponse(HttpStatusCode.OK, previewResult);

                return response;
            }
            catch (Exception exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception);
            }
        }

        private static bool IsValidURL(string url)
        {
            Uri uriResult;
            var result = Uri.TryCreate(url, UriKind.Absolute, out uriResult)
                         && ((uriResult.Scheme == Uri.UriSchemeHttp) || (uriResult.Scheme == Uri.UriSchemeHttps));

            return result;
        }
    }
}