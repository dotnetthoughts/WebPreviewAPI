﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPreviewAPI.Models
{
    public class Preview
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}